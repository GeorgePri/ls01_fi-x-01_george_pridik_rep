package Treppen;

import java.util.Scanner;

public class Treppen {
    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Wie hoch soll die Treppe sein? ");
        int h=myScanner.nextInt();
        System.out.println("Wie breit soll die Treppenstufe sein? ");
        int b=myScanner.nextInt();
        for(int i=1; i!=h+1; i++){
            int maxWidth=h*b;
            if((i*b)<maxWidth){
                for(int iterator2=maxWidth-i*b;iterator2>0;iterator2--){
                    System.out.print(" ");
                }
            }
            for(int iterator=b*i; iterator>0;iterator--){
                    System.out.print("*");
                }
                System.out.println("");
            }
        }
    }

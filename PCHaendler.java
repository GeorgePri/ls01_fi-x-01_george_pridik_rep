import java.util.Scanner;
public class PCHaendler {


    public static void main(String[] args) {
        // Benutzereingaben lesen
        String artikel = liesString();
        int anzahl = liesInt();
        double preis = liesdouble();
        double mwst = liesmwst();

        // Verarbeiten
        double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
        double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);
        rechnungsausgabe(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
    }
    public static String liesString() {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("was moechten Sie bestellen?");
        String artikel = myScanner.next();
        return artikel;
    }
    public static int liesInt() {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Geben Sie die Anzahl ein:");
        int anzahl = myScanner.nextInt();
        return anzahl;
    }
    public static double liesdouble() {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Geben Sie den Nettopreis ein:");
        double preis = myScanner.nextDouble();
        return preis;
    }
    public static double liesmwst() {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
        double mwst = myScanner.nextDouble();
        return mwst;
    }
    public static double berechneGesamtnettopreis(int anzahl, double preis) {
        double nettogesamtpreis = anzahl * preis;
        return nettogesamtpreis;
    }
    public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
        double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
        return bruttogesamtpreis;
    }
    // Ausgeben
    public static void rechnungsausgabe(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
        System.out.println("\tRechnung");
        System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
        System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
    }
}
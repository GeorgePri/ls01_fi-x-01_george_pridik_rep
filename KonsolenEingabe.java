import java.util.Scanner;

public class KonsolenEingabe {
    public static void main(String[]args) {

        String name;
        int alter;
        float groeße;
        char geschlecht;

        Scanner myScanner= new Scanner(System.in);

        System.out.print("Guten Tag wir wollen alle ihre Daten! \n");
        System.out.print("Geben Sie ihren Namen ein: ");
        name = myScanner.next();
        System.out.println(name);

        System.out.print("Geben Sie ihr Alter an: ");
        alter = myScanner.nextInt();
        System.out.println(alter);

        System.out.print("Geben Sie ihr Größe an: ");
        groeße = myScanner.nextFloat();
        System.out.println(groeße);

        System.out.print("Geben Sie ihr Geschlecht (M/W/D) an: ");
        geschlecht = myScanner.next().charAt(0);
        System.out.println(geschlecht);

        myScanner.close();
    }
}

import java.util.Scanner;

public class Rom {
    public static void main(String[] args) {
        Scanner eingabe = new Scanner(System.in);
        String zeichen;
        System.out.print("Geben sie eins der Römischen Zeichen an (I, V, X, L, C, D, M):");
        zeichen = eingabe.next();
        switch(zeichen) {
            case "I":
                System.out.println("1");
                break;
            case "V":
                System.out.println("5");
                break;
            case "X":
                System.out.println("10");
                break;
            case "L":
                System.out.println("50");
                break;
            case "C":
                System.out.println("100");
                break;
            case "D":
                System.out.println("500");
                break;
            case "M":
                System.out.println("1000");
                break;
            default:
                System.out.println("Fehler!");
                break;
        }
        eingabe.close();
    }
}

import java.util.Scanner;

public class hardware {
    public static void main(String[] args) {

        Scanner eingabe = new Scanner(System.in);
        double zahl;
        double anzahl;
        double preis;
        int maus1 = 5;
        int maus2 = 15;

        System.out.println("Wählen Sie eine Maus aus.");
        System.out.println("Maus 1 ist eine normale Maus ohne weiter Features und kostet 5€ pro Stück.");
        System.out.println("Maus 2 ist fair Trade und kostet 15€ pro Stück.");
        System.out.println("");
        System.out.println("Bitte geben Sie mit 1 oder 2 an welche Maus Sie wählen.");
        System.out.print("Maus:");
        zahl = eingabe.nextInt();
        if (zahl == 1) {
            zahl = maus1;

        }
        if (zahl == 2) {
            zahl = maus2;

        }

        System.out.println("");
        System.out.println("Beachten Sie ab einem Kauf von 10 Mäusen entfallen die Lieferkosten von 10€.");
        System.out.print("Bitte geben Sie die Anzahl der Mäuse an die Sie kaufen wollen:");
        anzahl = eingabe.nextInt();
        if (anzahl <= 10) {
            preis = (zahl * anzahl + 10);
            preis = (preis / 100) * 107;
            System.out.printf("Der Preis beträgt %.2f €", preis);
        }
        if (anzahl > 10) {
            preis = (zahl * anzahl);
            preis = (preis / 100) * 107;
            System.out.printf("Der Preis beträgt %.2f€.", preis);
        }
        eingabe.close();
    }
}

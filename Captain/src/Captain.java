package Captain.src;

public class Captain {

	// Attribute
	private String surname;
	private int captainYears;
	private double gehalt;
	// TODO: 3. Fuegen Sie in der Klasse 'Captain' das Attribut 'name' hinzu und
	// implementieren Sie die entsprechenden get- und set-Methoden.
	private String name;

	// Konstruktoren

	// TODO:12. Implementieren Sie einen parameterlosen Konstrucktur und erzeugen
	// Sie ein weiteres Objekt cap6 mit dem parameterlosen Konstruktur.

	public Captain() {
//		this.surname = null;
//		this.name = null;
//		this.captainYears = 5555;
//		this.gehalt = 0.0;
	}

	// TODO: 4. Implementieren Sie einen Konstruktor, der die Attribute surname
	// und name initialisiert.
	public Captain(String name, String surname) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.surname = surname;
	}
	// TODO: 5. Implementieren Sie einen Konstruktor, der alle Attribute
	// initialisiert.

	public Captain(String name, String surname, int captainYears, double gehalt) {
		// TODO Auto-generated constructor stub
		this(name, surname);
		setCaptainYears(captainYears);
		setGehalt(gehalt);
	}

	// Verwaltungsmethoden
	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setName(String name) {
		this.name = name;

	}

	public String getName() {
		return this.name;

	}

	public void setGehalt(double gehalt) {

		// TODO: 1. Implementieren Sie die entsprechende set-Methode.
		// Ber�cksichtigen Sie, dass das Gehalt nicht negativ sein darf.

		if (gehalt >= 0) {
			this.gehalt = gehalt;
		} else {

			this.gehalt = 0;
		}

	}

	public double getGehalt() {
		// TODO: 2. Implementieren Sie die entsprechende get-Methode.
		return this.gehalt;
	}

	// TODO: 6. Implementieren Sie die set-Methode und die get-Methode f�r
	// captainYears.
	// Ber�cksichtigen Sie, dass das Jahr groesser als 2200 sein soll.

	public void setCaptainYears(int captainYears) {

		if (captainYears >= 2200) {

			this.captainYears = captainYears;

		} else {

			this.captainYears = 2200;
		}

	}

	public int getCaptainYears() {
		return captainYears;
	}

	// Weitere Methoden

	// TODO: 7. Implementieren Sie eine Methode 'vollname', die den vollen Namen
	// (Vor- und Nachname) als string zur�ckgibt.

	public String vollName() {

		return name + "" + surname;
	}

	@Override
	public String toString() { // overriding the toString() method

		// TODO: 8. Implementieren Sie die toString() Methode.

		return surname + " " + name + " " + captainYears + " " + gehalt;
	}

}
package Fahrkartenautomat;

import java.util.Locale;
import java.util.Scanner;

class Fahrkartenautomat {
    static Scanner tastatur = new Scanner(System.in);


    public static void main(String[] args) {

        double gesamtbetrag = 0;
        int zuZahlenderBetrag1 = 0;
        double zuZahlenderBetrag = 0;

        do {

            // Fahrkartenbestellung Erfassen
            zuZahlenderBetrag =  fahrkartenbestellung_Erfassen();

            // Berechnung
            zuZahlenderBetrag1 = (int) (zuZahlenderBetrag * 100);
            zuZahlenderBetrag = zuZahlenderBetrag1;

            // Anzahl der Tickets
            if  (zuZahlenderBetrag != 0) {
                gesamtbetrag = gesamtbetrag + anzahl_der_Tickets(zuZahlenderBetrag);
                System.out.printf("Zwischensumme Berechnung: %.2f \n\n", gesamtbetrag /100);
            }
        } while (zuZahlenderBetrag != 0);

        // Fahrkarten Bezahlen
        double eingezahlterGesamtbetrag = 0.0;
        double rueckgabebetrag = fahrkarten_Bezahlen(eingezahlterGesamtbetrag, gesamtbetrag);

        // Fahrschein Ausgabe
        if (gesamtbetrag>0) {
            fahrkarten_Ausgaben("\nFahrschein wird ausgegeben");

            // Wartezeit
            int millisekunde = 250;
            warte(millisekunde);

            // Rückgeld Ausgaben
            rueckgeld_Ausgaben(rueckgabebetrag);
        }
        // Scanner schliessen
        tastatur.close();
        // Benutzer Begrüßung
        begrueßung();

    }


    // Fahrkartenbestellung Erfassen
    public static double fahrkartenbestellung_Erfassen() {
        System.out.println("Wählen Sie Ihre Wunschfahrkarte für Belin AB aus: ");
        System.out.println("\n-- Einzelfahrschein Regeltarif AB [2,90 €] ---> Wählen Sie die 1");
        System.out.println("-- Tageskarte Regeltarif AB [8,60 €] ---> Wählen Sie die 2");
        System.out.println("-- Kleingruppen-Tageskarte Regeltarif AB [23,50 €] ---> Wählen Sie die 3");
        System.out.println("-- Bezahlen ---> Wählen Sie die 9");

        double zuZahlenderBetrag;
        while (true) {

            zuZahlenderBetrag = tastatur.nextDouble();

            if (zuZahlenderBetrag == 1) {
                zuZahlenderBetrag = 2.90;
                System.out.println("Sie haben Berlin AB Einzelfahrschein gewählt\n");
                break; }
            else if (zuZahlenderBetrag == 2) {
                zuZahlenderBetrag = 8.60;
                System.out.println("Sie haben Berlin AB Tageskarte gewählt\n");
                break; }
            else if (zuZahlenderBetrag == 3) {
                zuZahlenderBetrag = 23.50;
                System.out.println("Sie haben Berlin AB Kleingruppen-Tageskarte gewählt\n");
                break; }
            else if (zuZahlenderBetrag == 9) {
                zuZahlenderBetrag = 0;
                System.out.println("Sie haben Bezahlung gewählt");
                break; }

            else {
                System.out.printf("Ihre Wahl --> " + zuZahlenderBetrag);
                System.out.println(" ist Falsch!!! Bitte korrigieren Sie Ihre wahl! (nur 1 bis 3): ");

            }
        }
        return zuZahlenderBetrag;


    }
    //Anzahl der Ticket
    public static double anzahl_der_Tickets(double zuZahlenderBetrag) {
        System.out.print("Wählen Sie jetzt der Anzahl der Tickets: ");
        int anzahltickets = tastatur.nextInt();
        if (anzahltickets >0 && anzahltickets <10){
            zuZahlenderBetrag = anzahltickets * zuZahlenderBetrag;
            return zuZahlenderBetrag;
        }
        else {
            System.out.print("Achtung!!! Anzahl der Tickets soll zwischen 1 und 10 sein\n");
            anzahltickets = 1;
            System.out.print("Anzahl der Tickets wurde auf 1 gesetzt!!!\n");
            return zuZahlenderBetrag;
        }

    }
    // Fahrkarten Bezahlen
    public static double fahrkarten_Bezahlen(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {

        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.printf(Locale.US, "\nNoch zu zahlen: %.2f %s \n",
                    (zuZahlenderBetrag / 100 - eingezahlterGesamtbetrag / 100), "Euro");
            System.out.print("Eingabe (mind. 1Ct, höchstens 2 Euro): ");
            double eingeworfeneMuenze = tastatur.nextDouble();
            eingeworfeneMuenze = eingeworfeneMuenze * 100;
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }
        // Rückgeldberechnung und -Ausgabe
        // -------------------------------
        double rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        return rueckgabebetrag;
    }

    // Fahrkarten Ausgaben

    public static void fahrkarten_Ausgaben(String fahrschein_Text) {

        System.out.println(fahrschein_Text);
    }

    // Wartezeit
    public static void warte(int millisekunde) {

        for (int i = 0; i < 8; i++) {
            System.out.print("=");

            try {
                Thread.sleep(millisekunde);
            }

            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    // Rückgeld Ausgaben
    public static void rueckgeld_Ausgaben(double rueckgabebetrag) {
        if (rueckgabebetrag > 0.0) {
            System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO ", rueckgabebetrag / 100);
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            int rueckgabebetrag1 = (int) rueckgabebetrag;
            rueckgabebetrag = rueckgabebetrag1;

            // Münze Ausgaben
            while (rueckgabebetrag1 >= 200) {
                muenzeAusgeben(rueckgabebetrag1, "2 EURO");
                rueckgabebetrag1 -= 200;
            }
            while (rueckgabebetrag1 >= 100) {
                muenzeAusgeben(rueckgabebetrag1, "1 EURO");
                rueckgabebetrag1 -= 100;
            }
            while (rueckgabebetrag1 >= 50) {
                muenzeAusgeben(rueckgabebetrag1, "50 CENT");
                rueckgabebetrag1 -= 50;
            }
            while (rueckgabebetrag1 >= 20) {
                muenzeAusgeben(rueckgabebetrag1, "20 CENT");
                rueckgabebetrag1 -= 20;
            }
            while (rueckgabebetrag1 >= 10) {
                muenzeAusgeben(rueckgabebetrag1, "10 CENT");
                rueckgabebetrag1 -= 10;
            }
            while (rueckgabebetrag1 >= 5) {
                muenzeAusgeben(rueckgabebetrag1, "5 CENT");
                rueckgabebetrag1 -= 5;
            }
            while (rueckgabebetrag1 >= 2) {
                muenzeAusgeben(rueckgabebetrag1, "2 CENT");
                rueckgabebetrag1 -= 2;
            }
            while (rueckgabebetrag1 >= 1) {
                muenzeAusgeben(rueckgabebetrag1, "1 CENT");
                rueckgabebetrag1 -= 1;
            }
        }

    }

    // Münze Ausgaben
    public static void muenzeAusgeben(int betrag, String einheit) {
        System.out.println(einheit);
    }

    // Benutzer Begrüßung
    public static void begrueßung() {
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
                + "Wir wünschen Ihnen eine gute Fahrt.");
    }

}
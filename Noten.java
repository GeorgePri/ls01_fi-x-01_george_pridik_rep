import java.util.Scanner;

public class Noten {
    public static void main(String[] args) {
        Scanner eingabe = new Scanner(System.in);
        int note;
        System.out.print("Geben sie die Note von 1-6 ein:");
        note = eingabe.nextInt();
        switch(note) {
            case 1:
                System.out.println("Sehr gut");
                break;
            case 2:
                System.out.println("Gut");
                break;
            case 3:
                System.out.println("Befriedigend");
                break;
            case 4:
                System.out.println("Ausreichend");
                break;
            case 5:
                System.out.println("Mangelhaft");
                break;
            case 6:
                System.out.println("Ungenügend");
                break;
            default:
                System.out.println("Fehler!");
                break;
        }
        eingabe.close();
    }
}

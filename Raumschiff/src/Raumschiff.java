import java.util.ArrayList;

public class Raumschiff {

    // Attribute

    private int photonentorpedoAnzahl;
    private int energieversorgungInProzent;
    private int schildeInProzent;
    private int huelleInProzent;
    private int lebenserhaltungssystemInProzent;
    private int androidenAnzahl;
    private String schiffsName;

    private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
    private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

    // Konstruktoren

    Raumschiff() {
    }

    Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int zustandSchildeInProzent,
               int zustandHuelleInProzent, int zustandLebenserhaltungssystemInProzent, String schiffsName,
               int androidenAnzahl) {

        this.energieversorgungInProzent = energieversorgungInProzent;
        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
        this.schiffsName = schiffsName;
        this.huelleInProzent = zustandHuelleInProzent;
        this.schildeInProzent = zustandSchildeInProzent;
        this.lebenserhaltungssystemInProzent = zustandLebenserhaltungssystemInProzent;
        this.androidenAnzahl = androidenAnzahl;
    }

    // Verwaltungsmethoden

    public int getPhotonentorpedoAnzahl() {
        return photonentorpedoAnzahl;
    }

    public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
    }

    public int getEnergieversorgungInProzent() {
        return energieversorgungInProzent;
    }

    public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
        this.energieversorgungInProzent = energieversorgungInProzent;
    }

    public int getSchildeInProzent() {
        return schildeInProzent;
    }

    public void setSchildeInProzent(int schildeInProzent) {
        this.schildeInProzent = schildeInProzent;
    }

    public int getHuelleInProzent() {
        return huelleInProzent;
    }

    public void setHuelleInProzent(int huelleInProzent) {
        this.huelleInProzent = huelleInProzent;
    }

    public int getLebenserhaltungssystemInProzent() {
        return lebenserhaltungssystemInProzent;
    }

    public void setLebenserhaltungssystemInProzent(int lebenserhaltungssystemInProzent) {
        this.lebenserhaltungssystemInProzent = lebenserhaltungssystemInProzent;
    }

    public int getAndroidenAnzahl() {
        return androidenAnzahl;
    }

    public void setAndroidenAnzahl(int androidenAnzahl) {
        this.androidenAnzahl = androidenAnzahl;
    }

    public String getSchiffsName() {
        return schiffsName;
    }

    public void setSchiffsName(String schiffsName) {
        this.schiffsName = schiffsName;
    }

    // Methoden

    public void addLadund(Ladung neueLadung) {
        ladungsverzeichnis.add(neueLadung);

    }

    public void photonentorpedoSchiessen(Raumschiff r) {

    }

    public void phaserkanoneSchiessen(Raumschiff r) {
    }

    private void treffer(Raumschiff r) {
    }

    public void nachrichtAnAlle(Raumschiff r) {
    }

    public static ArrayList<String> eintraegeLogbuchZurueckgeben = new ArrayList<String>();

    public void photonentorpedosLaden(int anzahlTorpedos) {
    }

    public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle,
                                      int anzahlDroiden) {
    }

    public void zustandRaumschiff() {
    }

    public void ladungsverzeichnisAusgeben() {
    }

    public void ladungsverzeichnisAufraeumen() {
    }

    @Override
    public String toString() {
        return "Raumschiff [photonentorpedoAnzahl=" + photonentorpedoAnzahl + ", energieversorgungInProzent="
                + energieversorgungInProzent + ", schildeInProzent=" + schildeInProzent + ", huelleInProzent="
                + huelleInProzent + ", lebenserhaltungssystemInProzent=" + lebenserhaltungssystemInProzent
                + ", androidenAnzahl=" + androidenAnzahl + ", schiffsName=" + schiffsName + ", ladungsverzeichnis="
                + ladungsverzeichnis + "]";
    }

}
